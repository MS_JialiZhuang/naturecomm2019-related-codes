#! /usr/bin/python

import re
import os
import sys
import numpy as np
import pandas as pd
import scipy.stats as st
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import KMeans
from sklearn import preprocessing
from sklearn import decomposition

def normalize(tbl):
    df = pd.read_csv(tbl,sep='\t',index_col=0).iloc[:,:-1]
    df = df.loc[map(lambda x: not re.search(r'^ERCC-',x),df.index),:]
    df.index = map(lambda x: re.sub(r'\.\d+$','',x),df.index)
    print df.shape
    df = df.loc[df.iloc[:,:-1].apply(lambda x: max(x) > 50,axis=1),:]
    df = df.loc[df.iloc[:,:-1].apply(lambda x: (max(x)+1)/(min(x)+1) > 5,axis=1),:]
    df.iloc[:,:-1] = df.iloc[:,:-1].apply(lambda x: x/max(x),axis=1)
    return df
    

#this is an example for multiple myeloma Patient02; for other patients the exact same code applies
#*.congregated.tsv is a tab-delimited table with each row representing a gene and each column a sample
#H*.mat.tsv is a tab-delimited table contains the loadings for each gene across the NMF components; each row represents a component and each column represents a gene
def plotHeatmap():
    df = normalize('../MM02_output/MM02.congregated.tsv')
    names = df['Description']
    df = df[map(lambda x: not re.search(r'^IG[HLK]V',x),df['Description'])]
    H = pd.read_csv('../NMF_normalized/H_all_p10_a0.mat.tsv',sep='\t',index_col=0).astype('float').transpose()
    H = H.apply(lambda x: x/sum(x),axis=1)
    colors = ['red','blue','white','white','white','white','white','white','white','black']

    array = []
    cols = []
    for comp in H.columns:
        geneList = list(H[H[comp]>0.4].index)
        if len(geneList) < 10:
            continue
        df1 = df[map(lambda x: x in geneList,df.index)]
        g = sns.clustermap(df1.iloc[:,:-1],metric='euclidean',z_score=None,standard_scale=None,row_cluster=True,col_cluster=False,cmap="YlGnBu",vmax=1.0,vmin=0.0,square=False,yticklabels=False,figsize=(10,12))
        array.append(df1.iloc[g.dendrogram_row.reordered_ind,:])
        cols += [colors[comp]] * df1.shape[0]

    df = pd.concat(array,join='inner',axis=0).drop('Description',axis=1)
    df.columns = range(-2,16)
    #print df
    sns.set(context='talk',font_scale=0.9)
    fig,ax = plt.subplots(figsize=(16,15))
    sns.clustermap(df,cmap="YlGnBu",vmax=1.0,vmin=0,square=False,yticklabels=False,row_colors=cols,row_cluster=False,col_cluster=False)
    plt.xlabel('Days')
    plt.ylabel('Gene')
    plt.savefig('MM02.NMFcomp.timecourse.heatmap.pdf')
    plt.close()


def main():
    plotHeatmap()


if __name__=='__main__':
    main()
