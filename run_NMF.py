#! /usr/bin/python

import re
import os
import sys
import numpy as np
import pandas as pd
import scipy.stats as st
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.decomposition import NMF
from sklearn.decomposition import non_negative_factorization

#Read expression values from multiple myeloma patients
#*.congregated.tsv files are tab-delimited tables containing expression data with each row representing a gene and each column a sample (time point)
def getMatrix():
    df1 = pd.read_csv('../MM01_output/MM01.congregated.tsv',sep='\t',index_col=0).iloc[:,:-2]
    df2 = pd.read_csv('../MM02_output/MM02.congregated.tsv',sep='\t',index_col=0).iloc[:,:-2]
    df3 = pd.read_csv('../MM03_output/180507_APK105MM03BloodComp.congregated.tsv',sep='\t',index_col=0).iloc[:,:-2]
    df3 = df3.loc[:,map(lambda x: not re.search(r'_MM03_',x)==None,df3.columns)]
    df1.columns = map(lambda x: re.sub(r'^MM','MM01',x),df1.columns)
    df2.columns = map(lambda x: re.sub(r'^MM','MM02',x),df2.columns)
    df3.columns = map(lambda x: '-'.join([x.split('_')[1],x.split('_')[0]]),df3.columns)
    df = pd.concat([df1,df2,df3],join='inner',axis=1)
    df.index = map(lambda x: re.sub(r'\.\d+$','',x),df.index)
    df = df.loc[map(lambda x: not re.search(r'^ERCC-',x),df.index),:]
    df = df.apply(lambda x: x/sum(x)*1000000,axis=0)
    df = df.loc[df.apply(lambda x: any(x>50),axis=1),:]
    maxval = df.apply(max,axis=1)
    df = df.apply(lambda x: x/max(x)*100,axis=1)
    #df.to_csv('all.MM.norm.csv')
    return df,maxval


def applyNMF(df,alpha,p):
    mat = df.transpose()
    model = NMF(n_components=p,random_state=142,solver='cd',max_iter=1000000,alpha=alpha,l1_ratio=0,init='nndsvd')
    W = pd.DataFrame(data=model.fit_transform(mat),index=mat.index)
    H = pd.DataFrame(data=model.components_,columns=mat.columns)
    W.to_csv('W_all_p%d_a%d.mat.tsv' % (p,alpha),sep='\t')
    H.to_csv('H_all_p%d_a%d.mat.tsv' % (p,alpha),sep='\t')
    #print W
    #print H
    print model.n_iter_


def plotComp(Wmat):
    W = pd.read_csv(Wmat,sep='\t',index_col=0).astype('float')
    W['pid'] = map(lambda x: x.split('-')[0],W.index)
    W.index = map(lambda x: int(x.split('-')[1]),W.index)
    W = W.sort_index().iloc[:-1,:]
    W['day'] = range(-2,13) + range(-2,16) + range(-2,12)
    anno = {'0':'Erythrocyte','1':'Megakaryocyte','2':'Endothelial cell','3':'Mature neutrophil',
            '4':'T-cell','5':'NK cell','6':'Unknown','7':'Unknown','8':'Unknown','9':'Neutrophilic precursor'}

    sns.set(font_scale=1.8)
    from matplotlib.backends.backend_pdf import PdfPages
    pdf = PdfPages(Wmat.replace('.mat.tsv','.compTimeCourse2.pdf'))
    colors=['red','blue','purple']
    for col in W.columns[:-2]:
        df1 = W.pivot(index='day',columns='pid',values=col)
        #df1.plot(kind='line',lw=3,title='comp'+col,figsize=(15,12),fontsize=18)
        fig,ax = plt.subplots(figsize=(15,12))
        df1.plot(kind='line',lw=3.5,title=anno[col],ax=ax,fontsize=18,color=colors)
        for i in range(df1.shape[1]):
            df2 = pd.DataFrame(df1.iloc[:,i].dropna())
            df2['day'] = df2.index
            df2.plot('day',df1.columns[i],kind='scatter',use_index=True,s=100,color=colors[i],ax=ax)
        plt.ylabel('Coefficient')
        plt.savefig(pdf,format='pdf')
        plt.close()
        
    pdf.close()


def main():
    alpha = 0
    p = 10
    df,maxval = getMatrix()
    applyNMF(df,alpha,p)
    #plotComp('W_all_p%d_a%d.mat.tsv' % (p,alpha))



if __name__=='__main__':
    main()

