#! /usr/bin/bash

#Inputs are *.filtered.bam which are BAM files from STAR containing only concordantly mapped reads;
#and *.unAligned.out.bam which are also STAR output files containing un-aligned reads
#IG_TCR_genes.bed is the BED intervals for Ig and TCR genes
for i in *.filtered.bam 
do 
    name=${i/.filtered.bam/} 
    bedtools intersect -a $i -b /mnt/shares/Users/jzhuang/2017Jul_MM/IG_TCR_genes.bed -wa > $name.IG_TCR.bam
    bam2fastq -o $name.IG_TCR#.fastq $name.IG_TCR.bam
    bam2fastq -o $name.unAligned#.fastq $name"unAligned.out.bam"
    cat $name.IG_TCR_1.fastq $name.unAligned_1.fastq | sed '/^@/!d;s//>/;N' >> $name.candidate.reads.fa
    cat $name.IG_TCR_2.fastq $name.unAligned_2.fastq | sed '/^@/!d;s//>/;N' >> $name.candidate.reads.fa    
done
