#! /usr/bin/python

import re
import os
import sys
import glob
import math
import subprocess
import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

#map reads to the V(D)J regions (igBlastn) and constant regions (blastn) of the IG annotations
#the paths to the databases are hard coded here, the users need to adjust those paths according to their specific system
def preprocess(sample):
    subprocess.check_output('export IGDATA=/mnt/shares2/blast_db/', shell=True, stderr=subprocess.STDOUT)
    subprocess.check_output('export BLASTDB=/mnt/shares2/blast_db:/mnt/shares2/blast_db/database', shell=True, stderr=subprocess.STDOUT)
    if not os.path.exists('%s.igBLAST.results' % sample):
        subprocess.check_output('~/bin/ncbi-igblast-1.6.1/bin/igblastn -germline_db_V imgt_v1.fa -germline_db_J imgt_j1.fa -germline_db_D imgt_d1.fa -organism human -domain_system imgt -query %s.candidate.reads.fa -out %s.igBLAST.results -outfmt 7 -auxiliary_data /mnt/shares2/blast_db/optional_file/human_gl.aux -num_threads 8 -evalue 0.000001' % (sample,sample), shell=True, stderr=subprocess.STDOUT)

    if not os.path.exists('%s.BLAST.results' % sample):
        subprocess.check_output('blastn -query %s.candidate.reads.fa -task blastn -db imgtrefseq -out %s.BLAST.results -outfmt "6 qseqid sseqid scomnames qstart qend length pident evalue score" -evalue 0.000001 -num_threads 8 -max_hsps 2 -max_target_seqs 10' % (sample,sample), shell=True, stderr=subprocess.STDOUT)


#parsing blastn output (file name "BlastOut")
def parseBlastOutput0(BlastOut):
    anno = {}
    with open('/mnt/shares/Users/jzhuang/LIGM-DB/imgtrefseq.fasta','rU') as infile:
        for line in infile:
            if re.search(r'^>',line):
                a = line.split('|')
                if a[2] == 'Homo sapiens' and not a[4] == 'V-REGION' and not a[4] == 'D-REGION'\
                   and not a[4] == 'J-REGION':
                    b = a[0].split()
                    anno[b[0][1:]] = a[1]

    Cgene = {}
    CE = {}
    with open (BlastOut,'rU') as infile:
        for line in infile:
            a = line.split()
            a[7] = float(a[7])
            if not a[1] in anno: continue
            anno[a[1]] = anno[a[1]].split('*')[0]
            if not a[0] in CE or a[7] < CE[a[0]]: 
                CE[a[0]] = a[7]
                Cgene[a[0]] = set([anno[a[1]]])
            elif a[7] == CE[a[0]]: 
                Cgene[a[0]].add(anno[a[1]])

    return Cgene


#parsing igBlastn output (file name "igBlastOut")
def parseBlastOutput(igBlastOut):
    Vtop = {}
    Dtop = {}
    Jtop = {}
    VE = {}
    DE = {}
    JE = {}
    with open(igBlastOut,'rU') as infile:
        for line in infile:
            if not re.search(r'^[VDJ]',line): continue

            a = line.split()
            a[1] = a[1].replace('reversed|','')
            b = a[2].split('*')
            a[2] = b[0]
            if a[0] == 'V':
                if not a[1] in VE or float(a[12]) < VE[a[1]]:
                    Vtop[a[1]] = set([a[2]])
                    VE[a[1]] = float(a[12])
                elif float(a[12]) == VE[a[1]]:
                    Vtop[a[1]].add(a[2])
            if a[0] == 'D':
                if not a[1] in DE or float(a[12]) < DE[a[1]]:
                    Dtop[a[1]] = set([a[2]])
                    DE[a[1]] = float(a[12])
                elif float(a[12]) == DE[a[1]]:
                    Dtop[a[1]].add(a[2])
            if a[0] == 'J':
                if not a[1] in JE or float(a[12]) < JE[a[1]]:
                    Jtop[a[1]] = set([a[2]])
                    JE[a[1]] = float(a[12])
                elif float(a[12]) == JE[a[1]]:
                    Jtop[a[1]].add(a[2])
                
    return [Vtop,Dtop,Jtop]


#quantify read counts for the IG transcripts
def quantify(sampleName,IGgenes):
    IGexpr = {}
    uniq = {}
    for top in IGgenes:
        for contig,matches in top.iteritems():
            for match in matches:
                value = 1/float(len(matches))
                if match in IGexpr:
                    IGexpr[match] += value
                else:
                    IGexpr[match] = value

                if len(matches) == 1:
                    if match in uniq:
                        uniq[match] += 1
                    else:
                        uniq[match] = 1


    with open(sampleName+'.IGgenes.readCounts','w') as out:
        for key in sorted(IGexpr,key=IGexpr.get,reverse=True):
            value = int(IGexpr[key])
            if value > 0:
                if not key in uniq: uniq[key] = 0
                out.write('\t'.join([key,str(value),str(uniq[key])])+'\n')

    consolid = {}
    for key,value in IGexpr.iteritems():
        if re.search(r'^IG[HKL][VDJ]',key):
            gene = key.split('-')[0]
            if gene in consolid:
                consolid[gene] += value
            else:
                consolid[gene] = value
    print consolid
    with open(sampleName+'.IGgenes.consolid.counts','w') as out:
        for key,value in consolid.iteritems():
            out.write('%s\t%d\n' % (key,value))


def main():
    files = glob.glob('*.candidate.reads.fa')
    for fn in files:
        sample = fn.replace('.candidate.reads.fa','')
        preprocess(sample)
        IGgenes = parseBlastOutput(sample+'.igBLAST.results')
        IGgenes.append(parseBlastOutput0(sample+'.BLAST.results'))
        quantify(sample,IGgenes)


if __name__=='__main__':
    main()
