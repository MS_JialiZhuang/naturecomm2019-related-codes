# NatureComm2019 related codes


-get_IG_candidateReads.sh: take STAR alignment output BAM files and output candidate reads that are potentially from Ig/TCR genes

-Ig_mapping_quantification.py: map Ig/TCR reads to IMGT reportoirs of Ig variants; quantification

-timecourse.py: calculate and plot relative fractions of different Ig variants

-run_NMF.py: run non-negative matrix factorization on bone marrow transplant patient cf-mRNA expression data

-plotNMFheatmap.py: plot heatmap of different NMF components across time points