#! /usr/bin/python

import re
import os
import sys
import glob
import numpy as np
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import numpy.polynomial.polynomial as poly
import seaborn as sns

##################################################################
#This is the script used to calculate the relative fraction of Ig 
#V region variants and to plot their temporal changes in multiple 
#myeloma Patient 02 (Figure 2d). For other patients the same code 
#was used. Only need to adjust the corresponding files.
##################################################################

def getVmat1(Vgene):
    files = glob.glob('*.IGgenes.readCounts')
    data = {'Vgene':{}, 'total':{}}
    for fn in files:
        with open(fn,'rU') as infile:
            total = 0
            m = 0
            sample = fn.replace('.IGgenes.readCounts','')
            if re.search(r'-IP0',sample):
                sample = 'MM-'+sample.split('-')[0]

            for line in infile:
                a = line.split()
                if re.search(r'^%s' % Vgene[:4],a[0]):
                    a[2] = int(a[2])
                    total += a[2]
                    if Vgene == a[0]:
                        m += a[2]
            data['Vgene'].update({sample: m})
            data['total'].update({sample: total})

    df = pd.DataFrame.from_dict(data)
    df['ratio'] = df['Vgene']/df['total']
    return df
    

def plotLine1(Vgene):
    df = getVmat1(Vgene)
    df['days'] = range(-2,16)
    return df


def plotLineAll1():
    df1 = plotLine1('IGHV3-15')
    df2 = plotLine1('IGKV2-24')
    df = pd.concat([df1['ratio'],df2['ratio']],join='inner',axis=1)
    df.columns = ['IGHV3-15','IGKV2-24']
    df['days'] = range(-2,16)

    IGHVwt = ['IGHV1-18','IGHV1-46','IGHV1-3']
    IGKVwt = ['IGKV1-5','IGKV3-15']
    dfs = []
    for gene in IGHVwt + IGKVwt:
        tmp = plotLine1(gene)
        dfs.append(tmp['ratio'])
    df3 = pd.concat(dfs,join='outer',axis=1).fillna(0)
    df3.columns = IGHVwt + IGKVwt
    df3.index = range(-2,16)

    poly1 = poly.Polynomial(poly.polyfit(df[~(df['days']==3)]['days'],df[~(df['days']==3)]['IGHV3-15'],8))
    poly2 = poly.Polynomial(poly.polyfit(df['days'],df['IGKV2-24'],10))
    intvals = map(lambda x: x/10.0,range(-20,151))
    fit = pd.DataFrame({'days': intvals, 'IGHV3-15': poly1(intvals), 'IGKV2-24': poly2(intvals)})
    #df['HVfit'] = df.apply(lambda x: poly1(x['days']), axis=1)
    #df['KVfit'] = df.apply(lambda x: poly2(x['days']), axis=1)
    sns.set(font_scale=1.6,style='white',context='talk')
    fig,ax = plt.subplots(figsize=(15,12))
    df[['IGHV3-15','days']].plot('days','IGHV3-15',kind='line',lw=5,color='blue',ax=ax,fontsize=30)
    df[['IGKV2-24','days']].plot('days','IGKV2-24',kind='line',lw=5,color='red',ax=ax,fontsize=30)
    df.plot('days','IGHV3-15',kind='scatter',color='blue',ax=ax,fontsize=30,s=200)
    df.plot('days','IGKV2-24',kind='scatter',color='red',ax=ax,fontsize=30,s=200)
    #fit.plot('days','IGHV3-15',kind='line',lw=4.5,color='blue',ax=ax,fontsize=18,alpha=0.5)
    #fit.plot('days','IGKV2-24',kind='line',lw=4.5,color='red',ax=ax,fontsize=18,alpha=0.5)
    df3[IGHVwt].plot(kind='line',lw=4,ls='--',ax=ax,fontsize=30)
    df3[IGKVwt].plot(kind='line',lw=4,ls='-.',ax=ax,fontsize=30)
    plt.title('MM02 Immunoglobulin V gene fraction time course')
    plt.ylabel('Fraction')
    plt.xlabel('Days')
    plt.ylim(-0.1,1.0)
    plt.xlim(-2.5,15.5)
    plt.savefig('MM02.ratio.timecourse.plasma.pdf')
    plt.close()

    
def main():
    pd.options.display.max_rows = 100
    plotLineAll1()


if __name__=='__main__':
    main()
